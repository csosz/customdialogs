package com.csosz.customdialogs

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.LayoutRes
import com.google.android.material.dialog.MaterialAlertDialogBuilder

/**
 * Created by csosz.andras on 2019-05-22
 */
open class CustomViewDialog : BaseCustomDialog<View>() {

    override fun show(): CustomViewDialog {
        context?.let {
            val builder = MaterialAlertDialogBuilder(it)
            builder.setView(rootView)
                .setCancelable(cancelable)
                .setOnCancelListener { dialog ->
                    rootView.hideKeyboard()
                    cancelListener?.onCancel(dialog)
                }
                .setTitle(title)

            setupButtons(builder)
            alertDialog = builder.create()
            alertDialog.show()
            setupListener()
        }
        return this
    }

    private fun setView(rootView: View) {
        this.rootView = rootView
    }

    open class Builder(context: Context) : BaseBuilder<Builder>(context) {
        lateinit var root: View

        fun setView(@LayoutRes view: Int): Builder {
            return setView(LayoutInflater.from(context).inflate(view, null, false))
        }

        fun setView(root: View): Builder {
            this.root = root
            return this
        }

        private fun create(): CustomViewDialog {
            val dialog = build(CustomViewDialog())
            dialog.rootView = root
            dialog.show()
            return dialog
        }

        fun show(): CustomViewDialog {
            return create()
        }
    }
}