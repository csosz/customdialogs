package com.csosz.customdialogs

import android.content.Context
import android.view.LayoutInflater
import android.widget.TextView

/**
 * Created by csosz.andras on 2018. 11. 13.
 */
class ProgressDialog {

    private var dialog: CustomViewDialog? = null

    fun show(
        context: Context, cancelable: Boolean = false, text: String? = null
    ): CustomViewDialog? {
        if (dialog != null) return dialog
        return CustomViewDialog.Builder(context).apply {
            text?.let {
                val root =
                    LayoutInflater.from(context).inflate(R.layout.dialog_progress, null, false)
                root.findViewById<TextView>(android.R.id.text1).text = text
                setView(root)
            } ?: setView(R.layout.dialog_progress)
        }.setCancelListener { dialog = null }
            .setCancelable(cancelable).show().apply {
                dialog = this
            }

    }

    fun hide() {
        dialog?.dismiss()
        dialog = null
    }


}