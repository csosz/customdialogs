package com.csosz.customdialogs

import android.content.Context
import android.content.DialogInterface
import android.os.IBinder
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import com.google.android.material.dialog.MaterialAlertDialogBuilder

/**
 * Created by csosz.andras on 2018. 01. 26
 */
abstract class BaseCustomDialog<V : View> {
    protected lateinit var rootView: V
    protected lateinit var alertDialog: AlertDialog
    protected var listener: CustomDialogClickListener? = null
    protected var cancelListener: DialogInterface.OnCancelListener? = null
    protected var title: String? = null
    protected var positive: String? = null
    protected var negative: String? = null
    protected var neutral: String? = null
    protected var context: Context? = null
    protected var cancelable: Boolean = false

    abstract fun show(): BaseCustomDialog<*>

    protected fun setButtons(pos: String?, neg: String?, neut: String?) {
        this.positive = pos
        this.negative = neg
        this.neutral = neut
    }

    protected fun setupButtons(builder: MaterialAlertDialogBuilder) {
        if (positive != null)
            builder.setPositiveButton(positive, null)
        if (negative != null)
            builder.setNegativeButton(negative, null)
        if (neutral != null)
            builder.setNeutralButton(neutral, null)
    }

    protected fun setupListener() {
        val positive: Button? = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE)
        val negative: Button? = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE)
        val neutral: Button? = alertDialog.getButton(DialogInterface.BUTTON_NEUTRAL)

        positive?.setOnClickListener(getClickListener(DialogInterface.BUTTON_POSITIVE))
        negative?.setOnClickListener(getClickListener(DialogInterface.BUTTON_NEGATIVE))
        neutral?.setOnClickListener(getClickListener(DialogInterface.BUTTON_NEUTRAL))
    }

    private fun getClickListener(which: Int): View.OnClickListener {
        return View.OnClickListener {
            hideKeyboard(rootView.windowToken)
            if (listener?.onClicked(rootView, which) == true) {
                dismiss()
            }
        }
    }

    private fun hideKeyboard(windowToken: IBinder?) {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(windowToken, 0)
    }

    fun dismiss() {
        alertDialog.dismiss()
    }

    @Suppress("UNCHECKED_CAST")
    open class BaseBuilder<out B : BaseBuilder<B>>(val context: Context) {
        private var title: String? = null
        private var pos: String? = null
        private var neg: String? = null
        private var neu: String? = null
        private var cancelable = true
        private var listener: CustomDialogClickListener? = null
        private var cancelListener: DialogInterface.OnCancelListener? = null

        fun setTitle(title: String?): B {
            this.title = title
            return this as B
        }

        fun setTitle(@StringRes title: Int): B {
            this.title = context.getString(title)
            return this as B
        }

        fun setButtons(@StringRes pos: Int, @StringRes neg: Int, @StringRes neut: Int): B {
            this.pos = if (pos != 0) context.getString(pos) else null
            this.neg = if (neg != 0) context.getString(neg) else null
            this.neu = if (neut != 0) context.getString(neut) else null
            return this as B
        }

        fun setButtons(pos: String?, neg: String?, neut: String?): B {
            this.pos = pos
            this.neg = neg
            this.neu = neut
            return this as B
        }

        fun setCancelable(cancelable: Boolean): B {
            this.cancelable = cancelable
            return this as B
        }

        fun setListener(listener: CustomDialogClickListener): B {
            this.listener = listener
            return this as B
        }

        fun setCancelListener(listener: DialogInterface.OnCancelListener): B {
            this.cancelListener = listener
            return this as B
        }

        protected fun <T : BaseCustomDialog<*>> build(d: T): T {
            d.context = context
            d.title = title
            d.setButtons(pos, neg, neu)
            d.cancelable = cancelable
            d.listener = listener
            d.cancelListener = cancelListener
            return d
        }
    }
}

interface CustomDialogClickListener {
    /**
     * Custom listener for dialog button clicks.
     *
     * @param root  The given view
     * @param which Which button pressed (int from DialogInterFace)
     * @return true if need to dismiss the dialog, otherwise false
     */
    fun onClicked(root: View, which: Int): Boolean
}