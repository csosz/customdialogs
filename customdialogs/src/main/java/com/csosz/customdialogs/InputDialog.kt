package com.csosz.customdialogs

import android.annotation.SuppressLint
import android.content.Context
import android.text.InputType
import android.text.TextUtils
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.StringRes
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import java.util.*

/**
 * Created by csosz.andras on 2018. 03. 10
 */
class InputDialog : BaseCustomDialog<LinearLayout>() {

    private var inputs: List<Input>? = null
    private var maxLines: Int = 5
    private var message: String? = null

    @SuppressLint("InflateParams")
    override fun show(): InputDialog {
        context?.let {
            val builder = MaterialAlertDialogBuilder(it)
            rootView =
                LayoutInflater.from(it).inflate(R.layout.dialog_input, null, false) as LinearLayout
            if (!TextUtils.isEmpty(message)) {
                val tv = TextView(rootView.context)
                tv.text = message
                rootView.addView(tv)
            }
            for (input in inputs ?: ArrayList()) {
                val view: TextInputLayout = LayoutInflater.from(rootView.context).inflate(
                    R.layout.view_text_input_layout,
                    null,
                    false
                ) as TextInputLayout
                setHint(input.hint, input.floatingHint, view)
                view.editText?.setText(input.defVal)
                view.tag = input.tag
                view.editText?.maxLines = maxLines
                setInputType(input.type, view)
                rootView.addView(view)
            }

            builder
                .setView(rootView)
                .setTitle(title)
                .setCancelable(cancelable)
                .setOnCancelListener { dialog ->
                    rootView.hideKeyboard()
                    cancelListener?.onCancel(dialog)
                }

            setupButtons(builder)

            alertDialog = builder.create()
            alertDialog.show()
            setupListener()
        }
        return this
    }

    private fun setHint(hint: String?, floatingHint: Boolean, view: TextInputLayout) {
        if (floatingHint) {
            view.hint = hint
        } else {
            view.isHintEnabled = false
            view.editText?.hint = hint
        }
    }

    private fun setInputType(type: String?, input: TextInputLayout) {
        val editText = input.editText
        if (editText == null || type == null) return
        when (type) {
            InputFieldType.number ->
                editText.inputType = InputType.TYPE_CLASS_NUMBER
            InputFieldType.textPassword -> {
                input.isPasswordVisibilityToggleEnabled = true
                editText.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            InputFieldType.textEmailAddress ->
                editText.inputType = InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        }
    }

    private class Input(
        val tag: String,
        val hint: String?,
        val type: String?,
        val defVal: String?,
        val floatingHint: Boolean
    )

    /**
     * Builder pattern for input dialog, so you can chain methods to setup
     */
    class Builder(context: Context) : BaseBuilder<Builder>(context) {
        private val inputs: ArrayList<Input> = ArrayList()
        private var maxLines = 1
        private var message: String? = null

        /**
         * Add an [Input] row to the dialog
         * TODO: can be added a maximum limit
         * @param tag          You can find input by tag
         * @param hint         hint of the input
         * @param type         type of the input @link [InputFieldType]
         * @param defValue     default value of the input
         * @param floatingHint true if need floating hint enabled
         */
        fun addInput(
            tag: String,
            hint: String?,
            type: String?,
            defValue: String?,
            floatingHint: Boolean = true
        ): Builder {
            this.inputs.add(Input(tag, hint, type, defValue, floatingHint))
            return this
        }

        /**
         * Set maxLines of inputs
         */
        fun setSingleLine(lines: Int): Builder {
            this.maxLines = lines
            return this
        }

        /**
         * Add description string above the inputs.
         *
         * @param text message text
         */
        fun setMessage(text: String): Builder {
            this.message = text
            return this
        }

        fun setMessage(@StringRes text: Int): Builder {
            return setMessage(context.getString(text))
        }

        /**
         * Set dialog params, then show it
         *
         * @return input dialog
         */
        fun show(): InputDialog {
            var dialog = InputDialog()
            dialog = build(dialog)
            dialog.inputs = inputs
            dialog.message = message
            dialog.maxLines = maxLines
            return dialog.show()
        }
    }
}

object InputFieldType {
    const val text = "text"
    const val number = "number"
    const val textPassword = "textPassword"
    const val textEmailAddress = "textEmailAddress"
}