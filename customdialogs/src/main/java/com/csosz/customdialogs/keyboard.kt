package com.csosz.customdialogs

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 * Created by csosz.andras on 2019-06-15
 */

internal fun View.hideKeyboard() {
    if (windowToken == null || context == null) return
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm?.hideSoftInputFromWindow(windowToken, 0)
}