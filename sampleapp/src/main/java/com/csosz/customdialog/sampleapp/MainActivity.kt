package com.csosz.customdialog.sampleapp

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.csosz.customdialog.sampleapp.databinding.ActivityMainBinding
import com.csosz.customdialogs.CustomDialogClickListener
import com.csosz.customdialogs.InputDialog
import com.csosz.customdialogs.InputFieldType
import com.csosz.customdialogs.ProgressDialog

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        binding.input.setOnClickListener {
            InputDialog.Builder(this)
                .setTitle("Input Dialog")
                .addInput("adsf", "asdf", InputFieldType.text, null, true)
                .setButtons("done", "cancel", " ? ")
                .setListener(object : CustomDialogClickListener {
                    override fun onClicked(root: View, which: Int): Boolean = true
                })
                .show()
        }
        binding.progress.setOnClickListener {
            ProgressDialog().show(this, true)
        }
        binding.progressCustom.setOnClickListener {
            ProgressDialog().show(this, true, "zuzu")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
